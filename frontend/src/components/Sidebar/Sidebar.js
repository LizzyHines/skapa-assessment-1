import React from 'react'
import "./Sidebar.css"
import {SidebarData} from "./SidebarData"
import LocalOfferIcon from '@mui/icons-material/LocalOffer';
import ArrowCircleRightIcon from '@mui/icons-material/ArrowCircleRight';

function Sidebar() {
    return (
        <div className="sidebar">
            <h3 className="title"><LocalOfferIcon/>Suits Unlimited</h3>
            <ul className="SidebarList">
                {SidebarData.map((val, key)=> {
                    return (
                        <li
                            className="row"
                            id={window.location.pathname === val.link ? "active" : ""}
                            key={key}
                            onClick={() => {
                                window.location.pathname = val.link;
                            }}
                        >
                            <div id="icon">{val.icon}</div>
                            <div id="title">{val.title}</div>
                        </li>
                    );
                })}
            </ul>

            <button id="pro-plan">
                <h3>Upgrade Your Plan, Pro Plan</h3>
                <div>
                    <ArrowCircleRightIcon/>
                </div>

            </button>

        </div>
    );
}

export default Sidebar;