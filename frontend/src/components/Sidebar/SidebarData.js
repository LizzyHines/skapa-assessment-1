import React from 'react'
import GroupIcon from '@mui/icons-material/Group';
import BorderColorIcon from '@mui/icons-material/BorderColor';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';
import CheckroomIcon from '@mui/icons-material/Checkroom';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import AssignmentIcon from '@mui/icons-material/Assignment';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';


export const SidebarData = [
    {
        icon: <GroupIcon/>,
        title: "Customers",
        link: "/customers",
    },
    {
        icon: <BorderColorIcon/>,
        title: "Orders",
        link: "/orders",
    },
    {
        icon: <WorkOutlineIcon/>,
        title: "Order Assignment",
        link: "/assignments",
    },
    {
        icon: <CheckroomIcon/>,
        title: "Workroom Orders",
        link: "/workroom",

    },
    {
        icon: <AccessTimeIcon/>,
        title: "Timecard",
        link: "/timecard",
    },
    {
        icon: <AssignmentIcon/>,
        title: "Reports",
        link: "/reports",
        iconClosed: <ArrowDropDownIcon/>,
        iconOpened: <ArrowDropUpIcon/>,

        subNav: [
            {
                title: "Go here!",
                link: "/reports/go-here"
            },
            {
                title: "Go here instead!",
                link: "/reports/here-instead"
            }

]
    },
    {
        icon: <ManageAccountsIcon/>,
        title: "Admin",
        link: "/admin",
        iconClosed: <ArrowDropDownIcon/>,
        iconOpened: <ArrowDropUpIcon/>,

        subNav: [
            {
                title: "Click me!",
                link: "/admin/click-me"
            },
            {
                title: "No, Click me!",
                link: "/admin/no-me"
            }
        ]
    }
];