import React from 'react'
import "./Cart.css"
import NoteAltIcon from '@mui/icons-material/NoteAlt';

export function CartItem(props) {
    const {cartItems, onRemove} = props;
    return (
        <>
            <div id="item">
                <NoteAltIcon/>
                <div>
                {cartItems.map((item) => (
                    <div key={item.id}>
                        <div>{item.name}</div>
                        <div>{item.price}</div>
                        <div>
                            <button onClick={() => onRemove(item)}>-</button>
                        </div>
                    </div>
                    ))}
                </div>
            </div>
            <div id="notes">
                <NoteAltIcon/><h3>Notes for Tailor</h3>
            </div>
        </>
    );
}