import React from 'react'
import {CartItem} from "./CartItem";
import AddIcon from '@mui/icons-material/Add';
import "./Cart.css"
import ContentCopyIcon from "@mui/icons-material/ContentCopy";
import SaveAsIcon from "@mui/icons-material/SaveAs";
import CloseIcon from "@mui/icons-material/Close";

export default function Cart(props) {
    const {cartItems} = props;
    const total = cartItems.reduce((a, c) => a + c.price * c.qty, 0);

    return (
        <>
            <div className="container">
                <div id="heading">
                    <h1>Zachary Spangler</h1>
                    <div>
                        <button>
                            <h5><AddIcon/>Rush Order</h5>
                        </button>
                        <button>
                            <h5><AddIcon/>Discount</h5>
                        </button>
                    </div>
                </div>
                <div className="order">
                    <h2>Order #11244</h2>
                    <button>
                        <AddIcon/>
                        <h5>Item</h5>
                    </button>
                </div>
                <container className="item">
                    <div>
                        <h6>Item A: Coat  |  Inside</h6>
                    </div>
                    <div id="row">
                        <ContentCopyIcon/>
                        <SaveAsIcon/>
                        <CloseIcon/>
                    </div>
                </container>
                <div>
                    {cartItems.length === 0 && <div>Please select your alterations.</div>}
                    <CartItem/>
                </div>
                {cartItems.length !== 0 && (
                    <div>
                        <h2>Order Total:</h2>
                        <div>${total.toFixed(2)}</div>
                    </div>
                )}
                <button id="submit">Submit Order</button>
            </div>
        </>
    )
}


