export const optionData = {
    options: [
        {
            id: '1',
            name: "Sleeves Shorten",
            caption: "Using the seam ripper, unpick the cuff seam.",
            price: "20"
        },
        {
            id: '2',
            name: "Sleeves Lengthen",
            caption: "Using the seam ripper, unpick the cuff seam.",
            price: "15"
        },
        {
            id: '3',
            name: "Sides In",
            caption: "Using the seam ripper, unpick the cuff seam.",
            price: "20"
        },
        {
            id: '4',
            name: "Sides Out",
            caption: "Using the seam ripper, unpick the cuff seam.",
            price: "20"
        },
        {
            id: '5',
            name: "Center Back",
            caption: "Using the seam ripper, unpick the cuff seam.",
            price: "15"
        },
        {
            id: '6',
            name: "Lower Collar",
            caption: "Using the seam ripper, unpick the cuff seam.",
            price: "20"
        }
    ],
};

export default optionData;