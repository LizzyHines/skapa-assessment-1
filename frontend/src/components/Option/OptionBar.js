import React from 'react';
import "./Option.css";
import Option from "./Option"


export default function OptionBar(props) {
    const {options, onAdd} = props;
    return (
        <div className="Option">

            <h1>Coat Alterations</h1>

            {options.map((option) => (
                <Option key={option.id} option={option} onAdd={onAdd}/>
            ))}

        </div>
    );
}


