import React from 'react'
import "./Option.css"

export default function Option(props) {
    const {option, onAdd} = props;
    return (
        <button onClick={onAdd} className="Option-button">
            <div>
                <h2>{option.name}</h2>
                <p className="alteration">{option.caption}</p>
            </div>
            <div className="price">
                ${option.price}
            </div>
        </button>
    )
};
