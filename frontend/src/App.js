import React from 'react'
import './App.css';
import Sidebar from "./components/Sidebar/Sidebar"
import OptionBar from "./components/Option/OptionBar"
import Cart from "./components/Cart/Cart"
import OptionData from "./components/Option/OptionData";
import {useState} from "react";

function App() {
    const {options} = OptionData;
    const [cartItems, setCartItems] = useState([]);
    const onAdd = (option) => {
        const exist = cartItems.find(x => x.id === option.id);
        if(exist) {
            setCartItems(cartItems.map((x) => x.id === option.id ? {...exist, qty: exist.qty} : x));
        } else {
            setCartItems([...cartItems, {...option, qty: 1}])
        }
    };
    const onRemove = (option) => {
        const exist = cartItems.find((x) => x.id === option.id);
        if(exist.qty === 1) {
            setCartItems(cartItems.filter((x) => x.id !== option.id));
        } else {
            setCartItems(
                cartItems.map((x) =>
                    x.id === option.id ? {...exist, qty: exist.qty - 1}: x)
            );
        }
    }

    return (
        <div className="App">
            <Sidebar/>
            <OptionBar onAdd={onAdd} options={options}/>
            <Cart onAdd={onAdd} onRemove={onRemove} cartItems={cartItems}/>
        </div>
    );
}

export default App;
